#define _POSIX_C_SOURCE 200809L

#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <netdb.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <microhttpd.h>
#include <sqlite3.h>

#include "ext.h"
#include "util.h"

#define stringify(arg) _stringify(arg)
#define _stringify(arg) #arg
#define log_error(...)                                                                  \
    fprintf(stderr, "[ERROR] " __FILE__ ":" stringify(__LINE__) " " __VA_ARGS__)
#define log_warn(...)                                                                   \
    fprintf(stderr, "[ WARN] " __FILE__ ":" stringify(__LINE__) " " __VA_ARGS__)
#define log_info(...)                                                                   \
    fprintf(stderr, "[ INFO] " __FILE__ ":" stringify(__LINE__) " " __VA_ARGS__)
#ifdef NDEBUG
#define log_debug(...)
#define must(cond) ((void)cond)
#else
#define log_debug(...)                                                                  \
    fprintf(stderr, "[DEBUG] " __FILE__ ":" stringify(__LINE__) " " __VA_ARGS__)
#define must(cond) assert(cond)
#endif

#define POST_PROCESSOR_BUFFER_SIZE 4096

struct CommandLineOptions {
    const char *directory;
    const char *database_uri;
    uint16_t port;
    bool print_base_url;
    const char *init_sql;
};

struct HandlerState {
    struct MHD_PostProcessor *pp;
    char *sql;
};

static int
parse_commandline_options(int argc, char *argv[], struct CommandLineOptions *opts);

static void register_signals(void);

static void termination_handler(int signum);

static void report_daemon_info(struct MHD_Daemon *daemon, bool print_base_url);

static sqlite3 *open_database(const char *uri, const char *init_sql_path);

static enum MHD_Result request_handler(
    void *cls,
    struct MHD_Connection *connection,
    const char *url,
    const char *method,
    const char *version,
    const char *upload_data,
    size_t *upload_data_size,
    void **con_cls
);

static enum MHD_Result post_data_iterator(
    void *cls,
    enum MHD_ValueKind kind,
    const char *key,
    const char *filename,
    const char *content_type,
    const char *transfer_encoding,
    const char *data,
    uint64_t off,
    size_t size
);

static void request_cleanup(
    void *cls,
    struct MHD_Connection *connection,
    void **con_cls,
    enum MHD_RequestTerminationCode toe
);

static enum MHD_Result
free_post_data(void *cls, enum MHD_ValueKind kind, const char *key, const char *value);

static int read_sql_file(const char *dirname, const char *filestem, char **output);

static enum MHD_Result
respond_error(struct MHD_Connection *connection, unsigned int status);

void database_log_error_adapter(void *context, int err, const char *msg);

static volatile sig_atomic_t terminated;

int main(int argc, char *argv[]) {
    int status;
    struct CommandLineOptions opts;
    struct MHD_Daemon *daemon;
    sqlite3 *database;

    status = EXIT_SUCCESS;

    register_signals();

    if (parse_commandline_options(argc, argv, &opts) != 0) {
        status = EXIT_FAILURE;
        goto done;
    }

    if (opts.directory != NULL) {
        if (chdir(opts.directory) == -1) {
            log_error("failed to change directories: %s\n", strerror(errno));
            status = EXIT_FAILURE;
            goto done;
        }
    }

    must(
        sqlite3_config(SQLITE_CONFIG_LOG, database_log_error_adapter, NULL) == SQLITE_OK
    );

    must(sqlite3_initialize() == SQLITE_OK);

    must(sqlite3_auto_extension((void (*)(void))sqlite3_ext_init) == SQLITE_OK);

    database = open_database(opts.database_uri, opts.init_sql);
    if (database == NULL) {
        status = EXIT_FAILURE;
        goto done;
    }

    log_debug("starting server\n");
    daemon = MHD_start_daemon(
        MHD_USE_AUTO | MHD_USE_ERROR_LOG,
        opts.port,
        NULL,
        NULL,
        &request_handler,
        database,
        MHD_OPTION_NOTIFY_COMPLETED,
        &request_cleanup,
        NULL,
        MHD_OPTION_END
    );
    if (daemon == NULL) {
        log_error("failed to start HTTP server\n");
        status = EXIT_FAILURE;
        goto done_database;
    }

    report_daemon_info(daemon, opts.print_base_url);

    while (!terminated) {
        if (MHD_run(daemon) != MHD_YES) {
            log_error("failed to run HTTP server\n");
            status = EXIT_FAILURE;
            goto done_daemon;
        }
    }

done_daemon:
    log_info("stopping server\n");
    MHD_stop_daemon(daemon);
done_database:
    log_debug("closing database\n");
    must(sqlite3_close(database) == SQLITE_OK);
    must(sqlite3_shutdown() == SQLITE_OK);
done:
    return status;
}

static int
parse_commandline_options(int argc, char *argv[], struct CommandLineOptions *opts) {
    int c;

    opts->directory = NULL;
    opts->port = 0;
    opts->print_base_url = false;
    opts->init_sql = NULL;

    while ((c = getopt(argc, argv, "C:p:ui:")) != -1) {
        switch (c) {
        case 'C':
            opts->directory = optarg;
            break;
        case 'p':
            opts->port = atoi(optarg);
            break;
        case 'u':
            opts->print_base_url = true;
            break;
        case 'i':
            opts->init_sql = optarg;
            break;
        case '?':
            log_error("unknown option: %c\n", optopt);
            return 1;
        }
    }

    if (optind == argc) {
        log_error("missing database file argument\n");
        return 1;
    }

    opts->database_uri = argv[optind++];

    for (; optind < argc; optind++) {
        log_warn("extra argument: %s\n", argv[optind]);
    }

    return 0;
}

static void register_signals(void) {
    struct sigaction action;
    action.sa_handler = termination_handler;
    sigemptyset(&action.sa_mask);
    action.sa_flags = SA_RESTART;

    if (sigaction(SIGINT, &action, NULL) != 0)
        log_warn("failed to register SIGINT handler: %s\n", strerror(errno));
    if (sigaction(SIGTERM, &action, NULL) != 0)
        log_warn("failed to register SIGTERM handler: %s\n", strerror(errno));

    action.sa_handler = SIG_IGN;

    if (sigaction(SIGPIPE, &action, NULL) != 0)
        log_warn("failed to register SIGPIPE handler: %s\n", strerror(errno));
}

static void termination_handler(int signum) {
    terminated = signum;
}

/*!
 * @brief Report relevant details about the server.
 * @param daemon
 */
static void report_daemon_info(struct MHD_Daemon *daemon, bool print_base_url) {
    int fd;
    struct sockaddr_storage addr;
    socklen_t len;
    char host[1025], service[32];
    int result;

    fd = MHD_get_daemon_info(daemon, MHD_DAEMON_INFO_LISTEN_FD)->listen_fd;

    len = sizeof(addr);
    result = getsockname(fd, (struct sockaddr *)&addr, &len);

    if (result != 0) {
        log_warn("unable to determine listening socket name: %s\n", strerror(errno));
        return;
    }

    result = getnameinfo(
        (struct sockaddr *)&addr,
        len,
        host,
        sizeof(host),
        service,
        sizeof(service),
        NI_NUMERICHOST | NI_NUMERICSERV
    );
    if (result != 0) {
        log_warn(
            "unable to determine listening socket name: %s\n",
            gai_strerror(result)
        );
        return;
    }

    log_info("listening http://%s:%s\n", host, service);
    if (print_base_url) {
        fprintf(stdout, "http://%s:%s\n", host, service);
        fflush(stdout);
    }
}

/*!
 * @brief Open the SQLite database at @p uri.
 * @param[in] uri The URI which should be opened.
 * @return An open SQLite database or `NULL`.
 */
static sqlite3 *open_database(const char *uri, const char *init_sql_path) {
    int result;
    sqlite3 *database;
    char *errmsg, *sql;

    log_debug("opening database: %s\n", uri);
    result = sqlite3_open_v2(
        uri,
        &database,
        SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_URI |
            SQLITE_OPEN_NOMUTEX,
        NULL
    );
    if (result != SQLITE_OK) {
        log_error("failed to open database: %s\n", sqlite3_errstr(result));
        goto error;
    }

    if (init_sql_path != NULL) {
        result = read_file(init_sql_path, &sql, NULL);
        if (result != 0) {
            log_error(
                "failed to read database initialization file: %s\n",
                strerror(result)
            );
            goto error_database;
        }

        result = sqlite3_exec(database, sql, NULL, NULL, &errmsg);
        if (result != SQLITE_OK) {
            log_error(
                "failed to initialize database: %s\n%s\n",
                sqlite3_errstr(result),
                errmsg
            );
            goto error_sql;
        }

        free(sql);
    }

    return database;

error_sql:
    free(sql);
error_database:
    sqlite3_close(database);
error:
    return NULL;
}

/*!
 * @brief Return the result of executing the SQL file named @p method within the
 * directory at @p url.
 *
 * The @p url is interpretted as a path to a directory relative to the current working
 * directory. Within that directory, a file corresponding to the @p method is executed.
 * The bind parameters of statements within that file are pulled from the request in the
 * form of post data or the URI query.
 *
 * @param cls
 * @param connection
 * @param url
 * @param method
 * @param version
 * @param upload_data
 * @param upload_data_size
 * @param con_cls
 * @return The result of processing the request.
 */
static enum MHD_Result request_handler(
    void *cls,
    struct MHD_Connection *connection,
    const char *url,
    const char *method,
    const char *version,
    const char *upload_data,
    size_t *upload_data_size,
    void **con_cls
) {
    (void)version;

    /* The handler state. */
    struct HandlerState *state;
    /* The response for the request. */
    struct MHD_Response *response;
    /* A pointer into the `sql` buffer for iterating through the statements. */
    const char *sql_head;
    /* The SQLite statement to which the URL refers. */
    sqlite3_stmt *stmt;
    /*
     * The kind of connection value which should be used for pulling bind
     * parameters.
     */
    enum MHD_ValueKind lookup_kind;
    /* A generic error code variable used in multiple places. */
    int err;
    /* The index and length of the statement bind parameters. */
    int idx, len;
    /*
     * A generic string variable used for both the key and value of the
     * extracted connection values.
     */
    const char *str;
    /* The pointer and length for the body string. */
    int body_len;
    const void *body;
    /* Whether or not to continue stepping through the SQL statements. */
    bool stepping;
    /* The internal result of processing the request. */
    enum MHD_Result result;

    response = NULL;

    /* Check if this is the first time the handler has been called for this request.*/
    state = *con_cls;
    if (state == NULL) {
        /* Allocate and initialize the handler state. */
        *con_cls = state = malloc(sizeof(struct HandlerState));
        if (state == NULL)
            return respond_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);

        state->pp = NULL;
        state->sql = NULL;

        /* Determine which method which is being requested. */
        if (strcmp(method, MHD_HTTP_METHOD_GET) == 0 ||
            strcmp(method, MHD_HTTP_METHOD_DELETE) == 0) {
            lookup_kind = MHD_GET_ARGUMENT_KIND;
        } else if (strcmp(method, MHD_HTTP_METHOD_POST) == 0 ||
                   strcmp(method, MHD_HTTP_METHOD_PUT) == 0 ||
                   strcmp(method, MHD_HTTP_METHOD_PATCH) == 0) {
            state->pp = MHD_create_post_processor(
                connection,
                POST_PROCESSOR_BUFFER_SIZE,
                post_data_iterator,
                connection
            );
            if (state->pp == NULL)
                return respond_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);
            return MHD_YES;
        } else {
            return respond_error(connection, MHD_HTTP_NOT_IMPLEMENTED);
        }
    } else {
        /*
         * Since the state was non-NULL, this is not the first time the handler has been
         * called for the request. No need to string compare the method because it is
         * guaranteed to be uploading data.
         */
        lookup_kind = MHD_POSTDATA_KIND;
    }

    /* Handle an uploaded chunk of data. */
    if (*upload_data_size > 0) {
        assert(state->pp != NULL);
        MHD_post_process(state->pp, upload_data, *upload_data_size);
        *upload_data_size = 0;
        return MHD_YES;
    }

    /* Read the file as a string. */
    assert(url[0] == '/');
    err = read_sql_file(url + 1, method, &state->sql);
    switch (err) {
    case 0:
        break;
    case ENONET:
        log_debug("file not found: %s\n", url);
        return respond_error(connection, MHD_HTTP_NOT_FOUND);
    default:
        log_error("internal error: %s\n", strerror(err));
        return respond_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);
    }

    /*
     * Iterate through the SQL file and execute every statement. The first result row
     * produced by any of the statements is queued as the body of the response.
     */
    sql_head = state->sql;
    for (stepping = true; stepping; sqlite3_finalize(stmt)) {
        /* Prepare the SQLite statement, advancing the `sql_head` to the next statement.
         */
        err = sqlite3_prepare_v2(cls, sql_head, -1, &stmt, &sql_head);
        if (err != SQLITE_OK) {
            log_error("internal error: %s\n", sqlite3_errstr(err));
            result = respond_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);
            break;
        }
        if (stmt == NULL)
            break;

        /* Bind all the parameters from the relevant MHD connection value. */
        /*
         * TODO: Handle request bodies in a streaming manner piping it into SQLite's
         * incremental blob interface where appropriate.
         */
        len = sqlite3_bind_parameter_count(stmt);
        for (idx = 1; idx <= len; idx++) {
            /* Get the name of the parameter for this index. */
            str = sqlite3_bind_parameter_name(stmt, idx);
            /* Require that the bind parameter name is not just `?`. */
            assert(strlen(str) > 1);
            /* Skip past the prefix (i.e.: `?AAA`, `:AAA`). */
            str += 1;
            /*
             * Reassign `str` to the connection value rather than the key. This could
             * either be pulled from the URI's query parameter or from the request body
             * depending upon the HTTP method which was called. This is permitted to be
             * `NULL` as that will gracefully coerce into an SQLite null value.
             */
            str = MHD_lookup_connection_value(connection, lookup_kind, str);
            /*
             * Bind the connection value to the bind parameter. The lifetime is
             * `SQLITE_STATIC` because the connection outlives the statement.
             */
            err = sqlite3_bind_text(stmt, idx, str, -1, SQLITE_STATIC);
            switch (err) {
            case SQLITE_OK:
                break;
            case SQLITE_TOOBIG:
                result = respond_error(connection, MHD_HTTP_CONTENT_TOO_LARGE);
                stepping = false;
                continue;
            default:
                log_error("internal error: %s\n", sqlite3_errstr(err));
                result = respond_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);
                stepping = false;
                continue;
            }
        }

        /* Run the statement. If it returns a result row, then queue that as the response
         * body. */
        err = sqlite3_step(stmt);
        switch (err) {
        case SQLITE_DONE:
            result = MHD_YES;
            break;
        case SQLITE_ROW:
            /*
             * Presently, it is assumed the statement has two result columns:
             * - The `Content-Type` as a text value.
             * - The body as a blob value.
             */
            assert(sqlite3_column_count(stmt) == 2);

            /* Extract the body as a blob. */
            /* TODO: Handle this by using SQLite's incremental blob interface. */
            body = sqlite3_column_blob(stmt, 1);
            if (body == NULL) {
                err = sqlite3_errcode(cls);
                /*
                 * `err` is `SQLITE_OK` if the returned column's value was actually a
                 * null SQLite value. That should never be the case when the SQL files
                 * are correctly written.
                 */
                assert(err != SQLITE_OK);
                log_error("internal error: %s\n", sqlite3_errstr(err));
                result = respond_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);
                stepping = false;
                continue;
            }
            body_len = sqlite3_column_bytes(stmt, 1);

            /* Create a response from the body. */
            response = MHD_create_response_from_buffer(
                body_len,
                (void *)body,
                MHD_RESPMEM_MUST_COPY
            );
            if (response == NULL) {
                result = respond_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);
                stepping = false;
                continue;
            }

            /* Add the result row's content type to the response headers. */
            result = MHD_add_response_header(
                response,
                "Content-Type",
                (const char *)sqlite3_column_text(stmt, 0)
            );
            if (result != MHD_YES) {
                result = respond_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);
                stepping = false;
                continue;
            }

            /* Queue the response to be sent. */
            result = MHD_queue_response(connection, MHD_HTTP_OK, response);
            if (result != MHD_YES) {
                result = respond_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);
                stepping = false;
                continue;
            }

            /* Deallocate the response. */
            MHD_destroy_response(response);

            /*
             * Require that the statement only produce that single result row. Aggregates
             * must be done within the statement with functions such as
             * `json_group_array`.
             */
            err = sqlite3_step(stmt);
            assert(err == SQLITE_DONE);
            break;
        default:
            log_error("internal error: %s\n", sqlite3_errstr(err));
            result = respond_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);
            stepping = false;
            continue;
        }
    }

    return result;
}

static enum MHD_Result post_data_iterator(
    void *cls,
    enum MHD_ValueKind kind,
    const char *key,
    const char *filename,
    const char *content_type,
    const char *transfer_encoding,
    const char *data,
    uint64_t off,
    size_t size
) {
    (void)filename;
    (void)content_type;
    (void)transfer_encoding;
    (void)off;

    char *owned_value, *owned_key;
    size_t key_len;

    assert(kind == MHD_POSTDATA_KIND);

    key_len = strlen(key);
    owned_key = strndup(key, key_len);
    if (owned_key == NULL)
        goto error;

    owned_value = strndup(data, size);
    if (owned_value == NULL)
        goto error_key;

    return MHD_set_connection_value_n(
        (struct MHD_Connection *)cls,
        kind,
        owned_key,
        key_len,
        owned_value,
        size
    );

error_key:
    free(owned_key);
error:
    return MHD_NO;
}

static void request_cleanup(
    void *cls,
    struct MHD_Connection *connection,
    void **con_cls,
    enum MHD_RequestTerminationCode toe
) {
    (void)cls;
    (void)connection;
    (void)toe;

    struct HandlerState *state = *con_cls;
    MHD_destroy_post_processor(state->pp);
    free(state->sql);
    free(state);

    MHD_get_connection_values(connection, MHD_POSTDATA_KIND, &free_post_data, NULL);
}

static enum MHD_Result
free_post_data(void *cls, enum MHD_ValueKind kind, const char *key, const char *value) {
    (void)cls;

    assert(kind == MHD_POSTDATA_KIND);

    free((char *)key);
    free((char *)value);

    return MHD_YES;
}

/*!
 * @brief Read the contents of the file named @p filestem with the extension `.sql`
 * located in @p dirname into a buffer.
 * @param[in] dirname The path to the directory. It is assumed to be trusted.
 * @param[in] filestem The stem of the file name which should be read. It is assumed to
 * be trusted.
 * @param[out] output The pointer into which the resulting buffer should be assigned. The
 * value is unchanged in the event of an error.
 * @return A copy of the `errno` at the time of an error.
 */
static int read_sql_file(const char *dirname, const char *filestem, char **output) {
    char path[PATH_MAX];

    snprintf(path, sizeof(path), "%s/%s.sql", dirname, filestem);

    return read_file(path, output, NULL);
}

/*!
 * @brief Respond to @p connection with the given HTTP @p status.
 * @param[in,out] connection
 * @param[in] status
 * @return The result of queuing the response.
 */
static enum MHD_Result
respond_error(struct MHD_Connection *connection, unsigned int status) {
    struct MHD_Response *response;
    enum MHD_Result result;

    response = MHD_create_response_from_buffer(0, (void *)"", MHD_RESPMEM_PERSISTENT);
    if (response == NULL)
        return MHD_NO;
    result = MHD_queue_response(connection, status, response);
    MHD_destroy_response(response);
    return result;
}

void database_log_error_adapter(void *context, int err, const char *msg) {
    (void)context;
    log_error("SQL error (%d) %s\n", err, msg);
}

#ifndef UTIL_H

/*!
 * @brief Read the contents of the file named @p path into a buffer.
 * @param[in] path The stem of the file name which should be read. It is assumed to
 * be trusted.
 * @param[out] output The pointer into which the resulting buffer should be assigned. The
 * value is unchanged in the event of an error. The caller owns the allocated memory.
 * @param[out] size The pointer into which the resulting buffer size should be assigned.
 * value is unchanged in the event of an error. If `NULL`, then the argument is ignored.
 * @return A copy of the `errno` at the time of an error.
 */
int read_file(const char *path, char **output, size_t *size);

#endif
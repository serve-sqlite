#define _POSIX_C_SOURCE 200809L

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int read_file(const char *path, char **output, size_t *size) {
    int fd;
    off_t len, idx;
    ssize_t read_result;
    char *buffer;
    int err;

    err = 0;

    fd = open(path, O_RDONLY);
    if (fd == -1) {
        err = errno;
        goto done;
    }

    len = lseek(fd, 0, SEEK_END);
    if (len == -1) {
        err = errno;
        goto done_file;
    }
    idx = lseek(fd, 0, SEEK_SET);
    if (idx == -1) {
        err = errno;
        goto done_file;
    }

    buffer = malloc(len + 1);
    if (buffer == NULL) {
        err = errno;
        goto done_file;
    }

    idx = 0;
    while (idx < len) {
        read_result = read(fd, buffer + idx, len - idx);

        if (read_result == -1) {
            if (errno == EINTR)
                continue;
            err = errno;
            goto error_buffer;
        }

        if (read_result == 0)
            break;

        idx += read_result;
    }
    buffer[idx] = '\0';

    *output = buffer;
    if (size != NULL)
        *size = idx;

    goto done_file;

error_buffer:
    free(buffer);

done_file:
    close(fd);
done:
    return err;
}

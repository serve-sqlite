SHELL = /bin/sh
PYTHON = python3

.SUFFIX:

TARGET = serve-sqlite

CFLAGS = -g
ALL_CFLAGS = -I. -I3rdparty -Wall -Wextra -std=c99 -MMD -MP -DSQLITE_CORE $(CFLAGS)

LDLIBS = -lsqlite3 -lmicrohttpd -lm -lmustach-cjson -lcjson

SRCS = $(wildcard *.c)
OBJS = $(patsubst %.c,%.o,$(SRCS))
DEPS = $(patsubst %.c,%.d,$(SRCS))

.PHONY: all
all: $(TARGET)

.PHONY: clean
clean:
	$(RM) $(OBJS) $(DEPS) $(TARGET)

.PHONY: check
check: $(TARGET)
	$(PYTHON) -m unittest discover -s tests

$(TARGET): $(OBJS)
	$(CC) $^ $(LDLIBS) -o $@

%.o: %.c
	$(CC) $(ALL_CFLAGS) -c $< -o $@

-include $(DEPS)
SELECT
    'text/html; charset=utf-8' AS "content_type",
    mustache(
        read_file('task/GET.mustache.html'),
        json_group_array(json_object(
            'id',
            Task."id",
            'title',
            Task."title",
            'desc',
            Task."desc",
            'assignee',
            Actor."name",
            'cost',
            Task."cost"
        ))
     ) AS "doc"
FROM Task
INNER JOIN Actor
ON Task."assignee" = Actor."id"
WHERE (:title IS NULL OR Task."title" LIKE :title)
AND (:desc IS NULL OR Task."desc" LIKE :desc)
AND (:min_cost IS NULL OR Task."cost" >= :min_cost)
AND (:max_cost IS NULL OR Task."cost" <= :max_cost)
AND (:id IS NULL OR Task."id" = :id);
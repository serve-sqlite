INSERT INTO Task("assignee", "title", "desc", "cost")
VALUES (:assignee, :title, :desc, :cost)
RETURNING
    'application/json' AS "content_type",
    json_object('id', "id") AS "body";
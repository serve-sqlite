INSERT INTO Actor("name", "capacity")
VALUES (:name, :capacity)
RETURNING
    'application/json' AS "content_type",
    json_object('id', "id") AS "body";
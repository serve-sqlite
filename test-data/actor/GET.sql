WITH
    ActorTasksDocument AS (
        SELECT
            Task."assignee" AS "assignee",
            sum(Task."cost") AS "cost",
            json_group_array(json_object(
                'id',
                Task."id",
                'title',
                Task."title",
                'desc',
                Task."desc",
                'cost',
                Task."cost"
            )) AS "doc"
        FROM Task
        GROUP BY Task."assignee"
    )
SELECT
    'text/html; charset=utf-8' AS "content_type",
    mustache(
        read_file('actor/GET.mustache.html'),
        json_group_array(json_object(
            'id',
            Actor."id",
            'name',
            Actor."name",
            'capacity',
            Actor."capacity",
            'workload',
            coalesce(ActorTasksDocument."cost", 0),
            'tasks',
            coalesce(json(ActorTasksDocument."doc"), json('[]'))
        ))
    ) AS "body"
FROM Actor
LEFT JOIN ActorTasksDocument
ON Actor."id" = ActorTasksDocument."assignee"
WHERE (:name IS NULL OR Actor."name" LIKE :name)
AND (:max_capacity IS NULL OR Actor."capacity" <= :max_capacity)
AND (:min_capacity IS NULL OR Actor."capacity" <= :min_capacity)
AND (:id IS NULL OR Actor."id" = :id);
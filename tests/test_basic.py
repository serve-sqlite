#!/usr/bin/env python3

import json
import os
import sys

from difflib import unified_diff
from functools import reduce
from pathlib import Path
from shutil import copyfileobj
from subprocess import Popen, PIPE, DEVNULL, TimeoutExpired
from unittest import TestCase
from urllib.parse import urljoin, urlencode
from urllib.request import urlopen, Request

TESTS_DIR = Path(__file__).parent
UPDATE_SNAPSHOTS = os.getenv("UPDATE_SNAPSHOTS") == "1"


class TestBasic(TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.proc = Popen(
            (
                "valgrind",
                "./serve-sqlite",
                "-C",
                "test-data",
                "-i",
                "init.sql",
                "-u",
                "file::memory:",
            ),
            stdout=PIPE,
        )
        self.base_url = self.proc.stdout.readline().decode().rstrip()

    def tearDown(self) -> None:
        self.proc.stdout.close()
        self.proc.terminate()
        try:
            self.proc.wait(1.0)
        except TimeoutExpired:
            self.proc.kill()
            self.proc.wait()
        super().tearDown()

    def url(self, *args) -> str:
        return reduce(urljoin, args, self.base_url)

    def assertSnapshot(self, path, snapshot) -> None:
        snapshot_path = TESTS_DIR.joinpath("snapshot", snapshot)

        req = Request(self.url(path))
        req.add_header("Accept", "text/html; charset=utf-8")
        resp = urlopen(req)

        if UPDATE_SNAPSHOTS:
            with open(snapshot_path, "wb") as fh, resp:
                copyfileobj(resp, fh)
            return

        with open(snapshot_path, "r") as fh, resp:
            different = False
            diff = unified_diff(list(fh), [l.decode() for l in resp])
            if diff:
                sys.stderr.writelines(diff)
            self.assertFalse(different)

    def assertGet(self, path, expected) -> None:
        req = Request(self.url(path))
        req.add_header("Accept", "application/json")
        with urlopen(req) as resp:
            self.assertEqual(json.load(resp), expected)

    def assertPost(self, path, data, expected) -> None:
        data = urlencode(data).encode()
        req = Request(self.url(path), data=data)
        req.add_header("Accept", "application/json")
        with urlopen(req) as resp:
            self.assertEqual(json.load(resp), expected)

    def test_basic(self) -> None:
        self.assertSnapshot("/actor", "actors-empty.html")
        self.assertPost("/actor", {"name": "Greg", "capacity": 10}, {"id": 1})
        self.assertSnapshot("/actor", "actors-one.html")
        self.assertPost(
            "/task",
            {
                "assignee": 1,
                "title": "Go To Sleep",
                "desc": "You're going to be tired tomorrow.",
                "cost": 9999,
            },
            {"id": 1},
        )
        self.assertSnapshot("/actor", "actors-with-task.html")
        self.assertSnapshot("/task", "tasks-one.html")

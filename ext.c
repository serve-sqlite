#include <assert.h>
#include <stdlib.h>

#include <cjson/cJSON.h>
#include <mustach/mustach-cjson.h>
#include <sqlite3ext.h>
SQLITE_EXTENSION_INIT1

#include "util.h"

static void sqlite3_ext_mustache(sqlite3_context *ctx, int nargs, sqlite3_value **args) {
    sqlite3 *database;
    const unsigned char *template, *json_str;
    cJSON *json;
    int err;
    char *result;
    size_t size;

    assert(nargs == 2);

    /* Extract the file path. */
    template = sqlite3_value_text(args[0]);
    if (template == NULL) {
        database = sqlite3_context_db_handle(ctx);
        err = sqlite3_errcode(database);
        /*
         * If `err` is `SQLITE_OK`, that means the application provided `NULL` for the
         * filepath which is a programming error.
         */
        assert(err != SQLITE_OK);
        sqlite3_result_error_code(ctx, err);
        goto done;
    }

    /* Extract the JSON text. */
    json_str = sqlite3_value_text(args[1]);
    if (json_str == NULL) {
        database = sqlite3_context_db_handle(ctx);
        err = sqlite3_errcode(database);
        /*
         * If `err` is `SQLITE_OK`, that means the application provided `NULL` for the
         * JSON which is a programming error.
         */
        assert(err != SQLITE_OK);
        sqlite3_result_error_code(ctx, err);
        goto done;
    }
    size = sqlite3_value_bytes(args[1]);

    /* Parse the JSON text as a tree for use by mustach. */
    json = cJSON_ParseWithLength((const char *)json_str, size);
    if (json == NULL) {
        sqlite3_result_error_code(ctx, SQLITE_INTERNAL);
        goto done;
    }

    /* Allocate the result of templating with mustach. */
    err = mustach_cJSON_mem((const char *)template, 0, json, 0, &result, &size);
    if (err != 0) {
        sqlite3_result_error_code(ctx, SQLITE_INTERNAL);
        goto done_json;
    }

    /* This transfers ownership over `template` to SQLite. */
    sqlite3_result_text64(ctx, result, size, &free, SQLITE_UTF8);

done_json:
    cJSON_Delete(json);
done:
}

static void
sqlite3_ext_read_file(sqlite3_context *ctx, int nargs, sqlite3_value **args) {
    sqlite3 *database;
    const unsigned char *filepath;
    char *contents;
    size_t size;
    int err;

    assert(nargs == 1);

    /* Extract the file path. */
    filepath = sqlite3_value_text(args[0]);
    if (filepath == NULL) {
        database = sqlite3_context_db_handle(ctx);
        err = sqlite3_errcode(database);
        /*
         * If `err` is `SQLITE_OK`, that means the application provided `NULL` for the
         * filepath which is a programming error.
         */
        assert(err != SQLITE_OK);
        sqlite3_result_error_code(ctx, err);
    }

    /* Read the template file from disk. */
    err = read_file((const char *)filepath, &contents, &size);
    if (err != 0) {
        err = SQLITE_INTERNAL;
        sqlite3_result_error_code(ctx, err);
    }

    sqlite3_result_blob64(ctx, contents, size, &free);
}

#ifdef _WIN32
__declspec(dllexport)
#endif
int sqlite3_ext_init(
    sqlite3 *db,
    char **pzErrMsg,
    const sqlite3_api_routines *pApi
) {
    (void)pzErrMsg;
    int rc = SQLITE_OK;
    SQLITE_EXTENSION_INIT2(pApi);
    rc = sqlite3_create_function_v2(
        db,
        "mustache",
        2,
        SQLITE_UTF8,
        NULL,
        sqlite3_ext_mustache,
        NULL,
        NULL,
        NULL
    );
    if (rc != SQLITE_OK)
        goto done;
    rc = sqlite3_create_function_v2(
        db,
        "read_file",
        1,
        SQLITE_UTF8,
        NULL,
        sqlite3_ext_read_file,
        NULL,
        NULL,
        NULL
    );
    if (rc != SQLITE_OK)
        goto done;
done:
    return rc;
}